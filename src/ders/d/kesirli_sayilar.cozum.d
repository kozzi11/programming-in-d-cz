Ddoc

$(COZUM_BOLUMU Kesirli Sayılar)

$(OL

$(LI Önceki bölümdeki hesap makinesi programındaki üç satırdaki $(C int)'leri $(C double) yapmak yeter:
)

---
        double birinci;
        double ikinci;

        // ...

        double sonuç;
---

$(LI
Problemde 5 yerine daha fazla sayı girilmesi istenseydi programın nasıl daha da içinden çıkılmaz bir hale geleceğini görüyor musunuz:
)
---
import std.stdio;

void main()
{
    double sayı_1;
    double sayı_2;
    double sayı_3;
    double sayı_4;
    double sayı_5;

    write("Sayı 1: ");
    readf(" %s", &sayı_1);
    write("Sayı 2: ");
    readf(" %s", &sayı_2);
    write("Sayı 3: ");
    readf(" %s", &sayı_3);
    write("Sayı 4: ");
    readf(" %s", &sayı_4);
    write("Sayı 5: ");
    readf(" %s", &sayı_5);

    writeln("İki katları:");
    writeln(sayı_1 * 2);
    writeln(sayı_2 * 2);
    writeln(sayı_3 * 2);
    writeln(sayı_4 * 2);
    writeln(sayı_5 * 2);

    writeln("Beşte birleri:");
    writeln(sayı_1 / 5);
    writeln(sayı_2 / 5);
    writeln(sayı_3 / 5);
    writeln(sayı_4 / 5);
    writeln(sayı_5 / 5);
}
---

)

Macros:
        SUBTITLE=Kesirli Sayılar Problem Çözümü

        DESCRIPTION=Kesirli Sayılar Problem Çözümü

        KEYWORDS=d programlama dili dersleri öğrenmek tutorial kesirli sayılar problem çözüm
