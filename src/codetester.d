/**
 * This program processes a Ddoc file to scrape the code examples and compile
 * them. Here are the rules of the game:
 *
 * o Code samples that do not have a main() are ignored.
 *
 * o Code samples that have $(DERLEME_HATASI) are ignored.
 *
 * o Code samples that import non-standard modules are ignored.
 *
 * o The code examples that have a main() but should not be compiled must be
 *   marked with $(CODE_DONT_TEST).
 *
 * o Certain lines of code can be commented-out in code compilation testing by
 *   $(CODE_COMMENT_OUT)
 *
 * o Any code sample can be named by $(CODE_NAME).
 *
 * o Code samples that are named by $(CODE_NAME) can be imported by other code
 *   samples by $(CODE_XREF).
 */

import std.stdio;
import std.string;
import std.regex;
import std.process;
import std.algorithm;
import std.array;
import std.exception;
import std.file;
import std.path;

enum TestResult { passed, failed, noTestRun }

struct TestOutcome
{
    TestResult result;
    string output;
    string input;
}

struct FileStatus
{
    TestResult lastTestResult;
    size_t totalPrograms;    // The number of sample programs actually
                             // compiled
}

/* This is for correct grammer in the test reports. */
string pluralFor(size_t n)
{
    return n == 1 ? "" : "s";
}

int main(string[] args)
{
    auto file = args[1];

    const FileStatus status = testCodes(file);

    final switch (status.lastTestResult) {
    case TestResult.passed:
        writefln("Compiled %s program%s",
                 status.totalPrograms, pluralFor(status.totalPrograms));
        break;

    case TestResult.failed:
        writeln("Failed");
        return 1;

    case TestResult.noTestRun:
        writeln("Nothing to test; skipped");
        break;
    }

    return 0;
}

/* Represents a scraped code section. */
struct CodeSection
{
    string name;
    size_t lineNumber;
    string[] lines;
    bool hasMain = false;
    bool hasNonStandardImport = false;
    bool expectedToFail = false;

    @property bool isStripped() const
    {
        return (lines.empty || (!lines.front.strip.empty &&
                                !lines.back.strip.empty));
    }
}

/* Renames main() presumably to move it out of the way of the actual
 * main(). */
auto main_renamed(const(string[]) lines)
{
    return lines.map!(line => line.replace("main", "RENAMED_main"));
}

CodeSection extractCodeSection(R)(ref R range,
                                  string fileName,
                                  ref size_t lineNumber,
                                  const(string[])[string] codeSections)
{
    /* Skip the opening code delimiter. */
    range.popFront();
    ++lineNumber;

    CodeSection result;

    while (true) {
        auto line = range.front;

        if (line.isCodeDelimiter) {
            /* This section ends here. Skip the closing code delimiter. */
            range.popFront();
            ++lineNumber;
            break;
        }

        // Use a do-while loop so that we catch leading and trailing empty lines
        do {
            line = processCodeLine(line, fileName, lineNumber,
                                   codeSections, result);
        } while (!line.empty);

        ++lineNumber;
        range.popFront();

        if (range.empty) {
            break;
        }
    }

    enforceWithContext(
        result.isStripped,
        format("Code section has leading or trailing empty lines: %-(%s\n%)",
               result.lines),
        fileName, lineNumber);

    result.lineNumber = lineNumber;

    return result;
}

void enforceWithContext(bool condition,
                        string message,
                        string fileName,
                        size_t lineNumber)
{
    if (!condition) {
        writefln("Error: %s:%s: %s", fileName, lineNumber, message);
        throw new Exception("Enforcement failed");
    }
}

/* Processes code lines by parsing CODE_NAME, expanding CODE_XREF, etc. */
char[] processCodeLine(char[] line,
                       string fileName,
                       size_t lineNumber,
                       const(string[])[string] codeSections,
                       ref CodeSection result)
{
    auto codeNameResult = line.findSplit("$(CODE_NAME ");

    if (!codeNameResult[1].empty) {
        /* This line introduces the name of this section. */

        enforceWithContext(result.name.empty,
                           "Multiple CODE_NAME tags", fileName, lineNumber);

        const closingParenResult = codeNameResult[2].findSplit(")");

        result.name = closingParenResult[0].strip.idup;

        enforceWithContext(
            !closingParenResult[2].empty,
            format("Rest of CODE_NAME line is empty: %s", line),
            fileName, lineNumber);

        line = closingParenResult[2].dup;

    } else {

        auto codeInsertResult = line.findSplit("$(CODE_XREF ");

        if (!codeInsertResult[1].empty) {
            /* This line cross-references another code section. */

            const closingParenResult =
                codeInsertResult[2].findSplit(")");

            const codeRef = closingParenResult[0].strip;

            enforceWithContext((codeRef in codeSections) !is null,
                               format("Undefined code section: %s", codeRef),
                               fileName, lineNumber);

            enforceWithContext(!closingParenResult[2].empty,
                               "CODE_XREF cannot be the last tag on a line",
                               fileName, lineNumber);

            // There may be other CODE_XREF on this line
            line = closingParenResult[2].dup;

            result.lines ~= format("// vvv $(CODE_XREF %s) vvv", codeRef);
            /* Insert the referenced code section after renaming its
             * main() function so that it doesn't conflict with the actual
             * main of this sample program. */
            result.lines ~= main_renamed(codeSections[codeRef]).array;
            result.lines ~= format("// ^^^ $(CODE_XREF %s) ^^^", codeRef);

        } else {
            /* This is an ordinary code line. */

            result.lines ~= line.dup;

            enum mainLineExpr = ctRegex!("(void|int) main[ ]*(.*)");

            if (matchFirst(line, mainLineExpr)) {
                /* Ok, this section has a main() function. */
                result.hasMain = true;
            }

            enum importLineExpr = ctRegex!(`.*import *(.*?)(\..*)*;`);
            line = line.replace("$(HILITE", "");
            auto importMatch = matchFirst(line, importLineExpr);

            if (!importMatch.empty) {
                const packageName = importMatch[1];

                if (!["std", "core"].canFind(packageName)) {
                    /* This section has an non-standard import. */
                    result.hasNonStandardImport = true;
                }
            }

            enum compFailureExpr = ctRegex!(`.*DERLEME_HATASI`);

            if (matchFirst(line, compFailureExpr)) {
                /* This section is expected to fail anyway. */
                result.expectedToFail = true;
            }

            line = [];
        }
    }

    return line;
}

/* Whether this is a Ddoc code section delimiter. */
bool isCodeDelimiter(const(char)[] line)
{
    return line == "---";
}

/* Tests the sample codes of the provided Ddoc file. */
FileStatus testCodes(string fileName)
{
    auto file = File(fileName, "r");

    string[][string] codeSections;
    size_t testCount = 0;
    size_t lineNumber = 1;

    char[][] range;

    while (true) {
        import std.conv;
        string line = file.readln.strip;
        if (file.eof) {
            break;
        }

        range ~= line.to!(char[]);
    }

    while (!range.empty) {
        auto line = range.front;

        if (line.isCodeDelimiter) {
            auto codeSection =
                extractCodeSection(range, fileName, lineNumber, codeSections);

            if (codeSection.hasNonStandardImport) {
                writefln("Code has non-standard import; skipping");
                continue;
            }

            if (!codeSection.name.empty && codeSection.name !in codeSections) {
                codeSections[codeSection.name] = codeSection.lines;
            }

            if (codeSection.hasMain) {
                const testOutcome = testProgram(fileName, codeSection.lines);

                const testResult = testOutcome.result;

                const goodOutcome = ((codeSection.expectedToFail &&
                                      (testResult == TestResult.failed))
                                     ||
                                     (!codeSection.expectedToFail &&
                                      (testResult == TestResult.passed)));

                if (!goodOutcome) {
                    writefln("Unexpected compilation outcome");
                    writefln("Result: %s", testOutcome.result);
                    writefln("Output:\n%s\n", testOutcome.output);
                    if (!testOutcome.input.empty) {
                        writefln("Input:\n%s", testOutcome.input);
                    }

                    writefln("Program:\n%-(%s\n%)", codeSection.lines);

                    writefln("Error: %s:%s:", fileName, codeSection.lineNumber);
                    return FileStatus(TestResult.failed, testCount);
                }

                ++testCount;
            }
        }

        if (!range.empty) {
            ++lineNumber;
            range.popFront();
        }
    }

    return FileStatus(testCount ? TestResult.passed : TestResult.noTestRun,
                      testCount);
}

/* Tests the provided program. */
TestOutcome testProgram(string fileName, string[] program)
{
    const preFileName = format("%s.pre_ddoc.d", fileName);
    const postFileName = format("%s.post_ddoc.d", fileName);

    /* We want to remove Ddoc markup from the code. */
    enum ddocMacros = [
        "\nMacros:",
        "DDOC=$(BODY)",
        "DDOC_COMMENT=",
        "HILITE=$0",
        "DERLEME_HATASI=",
        "D_CODE=$0",
        "RED=$0",
        "BLUE=$0",
        "GREEN=$0",
        "YELLOW=$0",
        "BLACK=$0",
        "WHITE=$0",
        "CODE_DONT_TEST=___CODE_DONT_TEST_$0",
        "CODE_COMMENT_OUT=// CODE_COMMENT_OUT $0",
        "ESCAPES=/&/&/ /</</ />/>/"
    ];

    /* Pre-process the file to remove all of the Ddoc markup from the sample
     * program. */

    auto preDdocFile = File(preFileName, "w");

    preDdocFile.writeln("Ddoc\n");
    preDdocFile.writeln("---");
    if (!program[0].canFind("module")) {
        preDdocFile.writeln("module main;");
    }
    preDdocFile.writefln("%-(%s\n%)", program);
    preDdocFile.writeln("import std.stdio;");  // tolerate this omission
    preDdocFile.writeln("---");
    preDdocFile.writefln("%-(%s\n%)", ddocMacros);
    preDdocFile.close();

    auto ddocResult =
        executeShell(format("dmd -Dd%s -Df%s %s",
                            postFileName.dirName, postFileName, preFileName));

    if (ddocResult.status) {
        return TestOutcome(TestResult.failed,
                           ddocResult.output,
                           preFileName.readText);
    }

    auto compResult =
        executeShell(format("dmd -c -de -w -od%s %s",
                            postFileName.dirName, postFileName));

    if (compResult.status) {
        return TestOutcome(TestResult.failed, compResult.output);
    }

    return TestOutcome(TestResult.passed);
}
